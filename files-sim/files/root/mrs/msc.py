#!/usr/bin/python3

import sys
import socket

########################################

def send(cmd):
    'Send command via UDS to VLS'

    uds = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    try:
        uds.connect('/tmp/vls.uds')
        uds.sendall(cmd)

    except socket.error, msg:
        print(msg, file = sys.stderr)

    finally:
        uds.close()

########################################

def usage():
    'Show usage'

    print('usage: to be done')

########################################

def show():
    'Command: show'

    send('show')

########################################

def valid_vlanid(val):
    'Validates VLANID'

    try:
        if int(val) > 0:
            return True
    except:
        return False

    return False

########################################

def valid_portspec(spec):
    'Validates port specifier'

    try:

        port = int(spec[0]) # may throw an exception
        mode = spec[1]

        if not (port >= 0 and port <= 4):
            return False

        if not (mode == 't' or mode == 'u'):
            return False

    except:
        return False

    return True

########################################

def add():
    'Command: add'

    if len(sys.argv) != 5:
        print('add: Wrong number of arguments', file = sys.stderr)
        return

    if not valid_vlanid(sys.argv[2]):
        print('add: Invalid VLAN %s' % sys.argv[2], file = sys.stderr)
        return

    if not valid_portspec(sys.argv[3]):
        print('add: Invalid port specification %s' % sys.argv[3], file = sys.stderr)
        return
        
    if not valid_portspec(sys.argv[4]):
        print('add: Invalid port specification %s' % sys.argv[4], file = sys.stderr)
        return

    send("add" + ' ' + sys.argv[2] + ' ' + sys.argv[3] + ' ' + sys.argv[4])

########################################

def main():
    'Entry point'

    if len(sys.argv) < 2:
        usage()
        sys.exit(1)

    if sys.argv[1] == 'show':
        show()
    elif sys.argv[1] == 'add':
        add()

########################################

main()
