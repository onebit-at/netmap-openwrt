#!/usr/bin/python3

################################################################################
##
## startup script for mrs and vls.
## executed via rc.local.
##
################################################################################

import subprocess
import time

################################################################################

def start_link(names):
    "dummy function doc string"

    for name in names:
        subprocess.call(["ip", "link", "set", name, "up", "promisc", "on"])

################################################################################

def startup_meshrocket():
    'Startup sequence with mrs and vls'

    # switch to patched driver:
    subprocess.call(["rmmod", "virtio-net"])
    subprocess.call(["modprobe", "virtio-net-netmap"])

    # configure interfaces
    subprocess.call(["ip", "addr", "add", ip + "/24", "dev", "eth0.99"])
    start_link(["eth0", "eth1", "eth2", "eth3"])

    # prepare start of mero-switch.
    # ensure empty fdb.conf if vmN.fdb doesn't exist.
    subprocess.call(["rm", "fdb.conf"])
    subprocess.call(["touch", "fdb.conf"])
    subprocess.call(["cp", "vm" + str(nodeid) + ".fdb", "fdb.conf"])

    # add default entry to fdb.conf.
    fdb_conf = open("fdb.conf", "a")
    fdb_conf.write("99;" + ip + ";" + mac + ";0;S\n")
    fdb_conf.close()

    # use subprocess.Popen() to spawn a new process without waiting for it.
    # the process for this script will be gone.

    # start vlan-switch
    subprocess.Popen("./vls")

    # wait a bit just to be sure that VLS has finished initialising of unix-domain-socket
    time.sleep(1)

    subprocess.call(["./msc.py", "add", "1000", "0u", "4t"])
    subprocess.call(["./msc.py", "add", "101", "1u", "4t"])
    subprocess.call(["./msc.py", "add", "102", "2u", "4t"])
    subprocess.call(["./msc.py", "add", "103", "3u", "4t"])
    subprocess.call(["./msc.py", "add", "98", "0t", "4t"])

    # start mero-switch.
    subprocess.Popen("./mrs")

    pass

################################################################################

def startup_enddevice():
    'Startup sequence without mrs and vls'
    
    pass

################################################################################

# read whole MAC and write it to 'mac'.
sysfsfile_mac = open("/sys/class/net/eth0/address", "r")
mac = sysfsfile_mac.read().strip()
sysfsfile_mac.close()

# extract second to last (index 4) number as decimal value.
nodeid = int(mac.split(":")[4], 16)
ip = "172.17.0." + str(nodeid)

if nodeid >= 10:
    startup_meshrocket()
else:
    startup_enddevice()
