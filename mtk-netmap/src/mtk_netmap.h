#include <bsd_glue.h>
#include <net/netmap.h>
#include <netmap/netmap_kern.h>
#include <netmap/netmap_mem2.h>

//
// buf - pointer to buffer memory
// dir - in which ring buf sits (value from enum rxtx)
// si - slot index
//
static void mtk_init_slot_buffer(char *buf, int dir, uint16_t si)
{
	int i;
	uint16_t *wbuf = (uint16_t *)buf; // word buffer

	for (i = 0; i < 1024; i++)
	{
		uint16_t val = (dir == NR_TX ? 0x8000 : 0x0000) | si;

		if (i == 0) // mark first word
			val |= 0x4000;
		else if (i + 1 >= 1024) // mark last word
			val |= 0x2000;

		*wbuf = htons(val);

		wbuf++; // increment one word
	}
}

//
// copied and adapted e1000_netmap_init_buffers()
//
static int mtk_netmap_init_buffers(struct fe_priv *priv)
{
	struct ifnet *ifp = priv->netdev;
	struct netmap_adapter *na = NA(ifp);
	struct netmap_slot *slot;
	struct fe_tx_ring *tx_ring = &priv->tx_ring;
	struct fe_rx_ring *rx_ring = &priv->rx_ring;
	unsigned int i, r, si;
	uint64_t paddr;
	char *addr;

	if (!nm_native_on(na)) // early exit if we are not in netmap mode
	{
		nm_prinf("mtk_netmap_init_buffers: not in netmap mode\n");
		return 0;
	}

	for (r = 0; r < na->num_rx_rings; r++)
	{
		slot = netmap_reset(na, NR_RX, r, 0);

		if (!slot)
		{
			nm_prinf("Skipping RX ring %d, netmap mode not requested\n", r);
			continue;
		}

		for (i = 0; i < na->num_rx_desc; i++)
		{
			si = netmap_idx_n2k(&na->rx_rings[r], i);

			addr = PNMB(na, slot + si, &paddr);

			mtk_init_slot_buffer(addr, NR_RX, si);

			// netmap_load_map(...)
			// E1000_RX_DESC(*rxr, i)->buffer_addr = htole64(paddr);
			rx_ring->rx_dma[i].rxd1 = paddr;
		}

#ifdef xxx
		rxr->next_to_use = 0;
		/* preserve buffers already made available to clients */
		i = rxr->count - 1 - nm_kr_rxspace(&na->rx_rings[0]);
		if (i < 0) // XXX something wrong here, can it really happen ?
			i += rxr->count;
		D("i now is %d", i);
		wmb(); /* Force memory writes to complete */
		writel(i, hw->hw_addr + rxr->rdt);
#endif // xxx

		wmb();

		rx_ring->rx_calc_idx = 0; // overwrite value from default driver initialization

		nm_prinf("rx ring initialized: RX_CALC_IDX0 %d\n", na->num_rx_desc - 1);
	}

	// initialize the tx ring(s)

	for (r = 0; r < na->num_tx_rings; r++) // num_tx_rings is always 1 (see mtk_netmap_attach)
	{
		slot = netmap_reset(na, NR_TX, r, 0);

		if (!slot)
		{
			nm_prinf("Skipping TX ring %d, netmap mode not requested\n", r);
			continue;
		}

		for (i = 0; i < na->num_tx_desc; i++)
		{
			si = netmap_idx_n2k(&na->tx_rings[r], i);

			addr = PNMB(na, slot + si, &paddr);

			mtk_init_slot_buffer(addr, NR_TX, si);

			// netmap_load_map(...)
			// E1000_TX_DESC(*txr, i)->buffer_addr = htole64(paddr);
			tx_ring->tx_dma[i].txd1 = paddr;

			if (i == 0 || i + 1 >= na->num_tx_desc) // log 1st and last descriptor
				nm_prinf("i: %d, si: %d, addr: %p, paddr: %llx\n", i, si, (void *)addr, paddr);
		}

		nm_prinf("tx ring initialized\n");
	}

	return 1;
}

//
// copied and adapted e1000_netmap_txsync()
//
static void fe_set_txd(struct fe_tx_dma *txd, struct fe_tx_dma *dma_txd);
static int mtk_netmap_txsync(struct netmap_kring *kring, int flags)
{
	struct netmap_adapter *na = kring->na;
	struct ifnet *ifp = na->ifp;
	struct netmap_ring *ring = kring->ring;
	u_int nm_i;  /* index into the netmap ring */
	u_int nic_i; /* index into the NIC ring */
	u_int n;
	u_int const lim = kring->nkr_num_slots - 1;
	u_int const head = kring->rhead;
	struct fe_tx_dma new_txd;
	struct fe_tx_dma *cur_txd;

	// device-specific data
	struct fe_priv *priv = netdev_priv(ifp);
	struct fe_tx_ring *tx_ring = &priv->tx_ring;

	rmb();

	// 1st part: process new packets to send.

	if (!netif_carrier_ok(ifp))
	{
		goto out;
	}

	nm_i = kring->nr_hwcur;

	/*
    nm_prinf("head: %d hwcur: %d hwtail: %d ", head, kring->nr_hwcur, kring->nr_hwtail);
*/

	if (nm_i != head) // there are new packets to send
	{
		nic_i = netmap_idx_k2n(kring, nm_i);

		for (n = 0; nm_i != head; n++)
		{
			struct netmap_slot *slot = &ring->slot[nm_i];
			u_int len = slot->len;
			uint64_t paddr;
			void *addr = PNMB(na, slot, &paddr);
			memset(&new_txd, 0, sizeof(new_txd));

			// cur_txd = current dma-ring descriptor record (nic_i will be advanced at the end of the loop)
			cur_txd = &tx_ring->tx_dma[nic_i];

			// init tx descriptor
			if (priv->soc->tx_dma)
			{
				priv->soc->tx_dma(&new_txd); // inits txd4
			}
			else
			{
				new_txd.txd4 = TX_DMA_DESP4_DEF;
			}

			NM_CHECK_ADDR_LEN(na, addr, len); // seems to be just debugging stuff

			if (slot->flags & NS_BUF_CHANGED) // address of buffer within slot has changed (this happens on zerocopy).
			{
				new_txd.txd1 = paddr;
			}
			else
			{
				new_txd.txd1 = cur_txd->txd1;
			}

			new_txd.txd2 = TX_DMA_PLEN0(len);

			// one descriptor can hold two segments TX_DMA_LS0 or TX_DMA_LS1 marks if one or both segments are used. for the time being we use just first segment.
			new_txd.txd2 |= TX_DMA_LS0;
			new_txd.txd3 = 0;

			// txd1 ... done (address segment 1)
			// txd2 ... done (length)
			// txd3 ... done (address segment 2)
			// txd4 ... done (?)

			fe_set_txd(&new_txd, cur_txd);

			slot->flags &= ~(NS_REPORT | NS_BUF_CHANGED); // unset both bit-flags

			netmap_sync_map(na, (bus_dma_tag_t)na->pdev, (bus_dmamap_t) &paddr, len, NR_TX); // sync cpu-memory (flush cached cpu writes (from user space) to memory) for later dma-transfer

			/*
            nm_prinf("nm: %d nic: %d txd1: %x buf: %s base=%08x max=%u ctx=%u dtx=%u ",
                nm_i,
                nic_i,
                cur_txd->txd1,
                nm_dump_buf(NMB(na, slot), len, 128, NULL),
                fe_reg_r32(FE_REG_TX_BASE_PTR0),
                fe_reg_r32(FE_REG_TX_MAX_CNT0),
                fe_reg_r32(FE_REG_TX_CTX_IDX0),
                fe_reg_r32(FE_REG_TX_DTX_IDX0));
*/

			nm_i = nm_next(nm_i, lim);
			nic_i = nm_next(nic_i, lim);
		}

		kring->nr_hwcur = head;

		wmb(); /* synchronize writes to the NIC ring */

		fe_reg_w32(head, FE_REG_TX_CTX_IDX0); // hopefully this will dma-transfer some packets to nic.
	}

	nic_i = fe_reg_r32(FE_REG_TX_DTX_IDX0);
	kring->nr_hwtail = nm_prev(netmap_idx_n2k(kring, nic_i), lim);

	/*
    nm_prinf("new hwtail: %d ", kring->nr_hwtail);
*/

#ifdef xxx

	// 2nd part: reclaim buffers for completed transmissions.

	if (flags & NAF_FORCE_RECLAIM || nm_kr_txempty(kring))
	{
		nic_i = fe_reg_r32(FE_REG_TX_DTX_IDX0);

		if (nic_i >= kring->nkr_num_slots) /* XXX can it happen ? */
		{
			D("TDH wrap %d", nic_i);
			nic_i -= kring->nkr_num_slots;
		}

		kring->nr_hwtail = nm_prev(netmap_idx_n2k(kring, nic_i), lim);
	}

#endif // xxx

out:

	return 0;
}

static void inline dump(struct fe_rx_dma *rxd, char *buf)
{
#ifdef xxx
	char hexdump[1024], *tmp;
	int i, sep;

	tmp = hexdump;
	for (i = 0; i < 50; i++)
	{
		sep = (i == 14 || i == 42); // separator: after ethernet and ip/udp header
		tmp += sprintf(tmp, "%s%02X", sep ? " " : "", (uint8_t)buf[i]);
	}
	*tmp = '\0';

	//nm_prinf("%08X %08X %08X %08X %s\n", rxd->rxd1, rxd->rxd2, rxd->rxd3, rxd->rxd4, hexdump);
	nm_prinf("%08X %s\n", buf, hexdump);
#endif // xxx

	u32 cnt = *(u32 *)(buf + 42 + 4); // skip header (ethernet, ip, udp) and skip vlan tag
	cnt = ntohl(cnt);
	nm_prinf("%8d\n", cnt);
}

//
// copied and adapted e1000_netmap_rxsync()
//
static void fe_get_rxd(struct fe_rx_dma *rxd, struct fe_rx_dma *dma_rxd);
static int mtk_netmap_rxsync(struct netmap_kring *kring, int flags)
{
	struct netmap_adapter *na = kring->na;
	struct ifnet *ifp = na->ifp;
	struct netmap_ring *nm_ring = kring->ring;
	u_int nm_i;  /* index into the netmap ring */
	u_int nic_i; /* index into the NIC ring */
	u_int n;
	u_int const lim = kring->nkr_num_slots - 1;
	u_int const head = kring->rhead;
	int force_update = (flags & NAF_FORCE_READ) || kring->nr_kflags & NKR_PENDINTR;
	struct fe_rx_dma *rxd, trxd;
	char *addr;

	/* device-specific */
	struct fe_priv *priv = netdev_priv(ifp);
	struct fe_rx_ring *ring = &priv->rx_ring;

	if (!netif_carrier_ok(ifp))
	{
		nm_prinf("check - netif_carrier_ok not ok\n");
		goto out;
	}

	if (head > lim)
	{
		nm_prinf("check - head %d > lim %d\n", head, lim);
		return netmap_ring_reinit(kring);
	}

	rmb();

	/*
	 * First part: import newly received packets.
	 */
	if (netmap_no_pendintr || force_update)
	{
		nic_i = ring->rx_calc_idx;
		nm_i = netmap_idx_n2k(kring, nic_i);

		for (n = 0;; n++) // loop until RX_DMA_DONE
		{
			struct netmap_slot *slot = &nm_ring->slot[nm_i];
			uint64_t paddr;
			addr = PNMB(na, slot, &paddr);

			rxd = &ring->rx_dma[nic_i]; // pointer to current rx-descriptor
			fe_get_rxd(&trxd, rxd);		// get copy of rx-descriptor

			if (!(trxd.rxd2 & RX_DMA_DONE))
				break;

			slot->len = RX_DMA_GET_PLEN0(trxd.rxd2);
			slot->flags = 0; // https://github.com/luigirizzo/netmap/issues/445

			netmap_sync_map(na, (bus_dma_tag_t)na->pdev, (bus_dmamap_t) &paddr, slot->len, NR_RX);

			// dump buffer via address of kseg1 (unmapped, uncacheable)
			// dump(&trxd, virt_to_phys(addr) + 0xA0000000);

			nm_i = nm_next(nm_i, lim);
			nic_i = nm_next(nic_i, lim);
		}

		if (n) // did we processed something? -> if yes, update the state variables!
		{
			ring->rx_calc_idx = nic_i;
			kring->nr_hwtail = nm_i;
		}

		kring->nr_kflags &= ~NKR_PENDINTR;
	}

	/*
	 * Second part: skip past packets that userspace has released.
	 */
	nm_i = kring->nr_hwcur;

	if (nm_i != head)
	{
		nic_i = netmap_idx_k2n(kring, nm_i);

		for (n = 0; nm_i != head; n++)
		{
			struct netmap_slot *slot = &nm_ring->slot[nm_i];
			uint64_t paddr;
			void *addr = PNMB(na, slot, &paddr);
			rxd = &ring->rx_dma[nic_i]; // pointer to current rx-descriptor

			if (addr == NETMAP_BUF_BASE(na)) // bad buf
			{
				goto ring_reset;
			}

			if (slot->flags & NS_BUF_CHANGED)
			{
				// netmap_reload_map(...)
				rxd->rxd1 = paddr;
				slot->flags &= ~NS_BUF_CHANGED;
			}

			// reset rxd2 (see mtk_eth_soc.c)

			if (priv->flags & FE_FLAG_RX_SG_DMA)
				rxd->rxd2 = RX_DMA_PLEN0(ring->rx_buf_size);
			else
				rxd->rxd2 = RX_DMA_LSO;

			nm_i = nm_next(nm_i, lim);
			nic_i = nm_next(nic_i, lim);
		}

		kring->nr_hwcur = head;

		// Now nic_i, head and hwcur point to the same slot.
		// Recalculate nic_i to get the previous slot of hwcur.
		nic_i = nm_prev(nic_i, lim);

		wmb();

		// Let RX_CALC_IDX0 point to one slot before hwcur.
		fe_reg_w32(nic_i, FE_REG_RX_CALC_IDX0);
	}

out:

	return 0;

ring_reset:

	return netmap_ring_reinit(kring);
}

//
//
//
static int fe_stop(struct net_device *dev);
static int fe_open(struct net_device *dev);
static int mtk_netmap_reg(struct netmap_adapter *na, int onoff)
{
	struct fe_priv *priv = netdev_priv(na->ifp);

	nm_prinf("mtk_netmap_reg - onoff %d\n", onoff);

	fe_stop(na->ifp);

	if (onoff)
	{
		nm_set_native_flags(na);
	}
	else
	{
		nm_clear_native_flags(na);
	}

	// during initialization FE_FLAG_RX_2B_OFFSET is set in priv->flags.
	// fe_open() checks FE_FLAG_RX_2B_OFFSET in priv->flags and activates it.
	// we don't want this feature so we unset it in priv->flags.

	if (priv->flags & FE_FLAG_RX_2B_OFFSET)
		nm_prinf("RX_2B_OFFSET is ON\n");

	if (onoff) // if we are in netmap mode...
	{
		// ... then unset feature flag

		nm_prinf("RX_2B_OFFSET -> OFF\n");
		priv->flags &= ~FE_FLAG_RX_2B_OFFSET;
	}
	else
	{
		nm_prinf("RX_2B_OFFSET -> ON\n");
		priv->flags |= FE_FLAG_RX_2B_OFFSET;
	}

	fe_open(na->ifp);

	return 0;
}

#ifdef not_used

static inline void fe_int_enable(u32 mask);
static inline void fe_int_disable(u32 mask);
static void mtk_netmap_intr(struct netmap_adapter *na, int onoff)
{
	struct fe_priv *priv = netdev_priv(na->ifp);

	if (onoff)
	{
		fe_int_enable(priv->soc->tx_int | priv->soc->rx_int);
	}
	else
	{
		fe_int_disable(priv->soc->tx_int | priv->soc->rx_int);
	}
}

#endif // not_used

static void mtk_netmap_attach(struct fe_priv *priv)
{
	struct netmap_adapter na;

	nm_prinf("mtk_netmap_attach\n");

	bzero(&na, sizeof(na));

	na.ifp = priv->netdev;						 /* struct net_device* */
	na.pdev = priv->device;						 /* struct device* */
	na.num_tx_desc = priv->tx_ring.tx_ring_size; /* na.num_tx_desc = NM_IXGBE_TX_RING(adapter, 0)->count; */
	na.num_rx_desc = priv->rx_ring.rx_ring_size; /* na.num_rx_desc = NM_IXGBE_RX_RING(adapter, 0)->count; */
	na.num_tx_rings = 1;						 /* na.num_tx_rings = adapter->num_tx_queues; */
	na.num_rx_rings = 1;						 /* na.num_rx_rings = adapter->num_rx_queues; */

	na.nm_txsync = mtk_netmap_txsync;
	na.nm_rxsync = mtk_netmap_rxsync;
	na.nm_register = mtk_netmap_reg;

	// na.nm_intr = mtk_netmap_intr; // only used by vale (?)

	netmap_attach(&na);
}

static void mtk_netmap_detach(struct net_device *dev)
{
	nm_prinf("mtk_netmap_detach\n");

	netmap_detach(dev);
}

struct reg_2_nam
{
	u16 reg;
	char *nam;
};

#ifdef NOT_USED

static const struct reg_2_nam reg_2_nam[] = {
	{RT5350_PDMA_GLO_CFG, "RT5350_PDMA_GLO_CFG"},
	{RT5350_PDMA_RST_CFG, "RT5350_PDMA_RST_CFG"},
	{RT5350_DLY_INT_CFG, "RT5350_DLY_INT_CFG"},
	{RT5350_TX_BASE_PTR0, "RT5350_TX_BASE_PTR0"},
	{RT5350_TX_MAX_CNT0, "RT5350_TX_MAX_CNT0"},
	{RT5350_TX_CTX_IDX0, "RT5350_TX_CTX_IDX0"},
	{RT5350_TX_DTX_IDX0, "RT5350_TX_DTX_IDX0"},
	{RT5350_RX_BASE_PTR0, "RT5350_RX_BASE_PTR0"},
	{RT5350_RX_MAX_CNT0, "RT5350_RX_MAX_CNT0"},
	{RT5350_RX_CALC_IDX0, "RT5350_RX_CALC_IDX0"},
	{RT5350_RX_DRX_IDX0, "RT5350_RX_DRX_IDX0"},
	{RT5350_FE_INT_ENABLE, "RT5350_FE_INT_ENABLE"},
	{RT5350_FE_INT_STATUS, "RT5350_FE_INT_STATUS"},
	// MT7621_XXX are defined in soc_mt7621.c and therefor not includeable :( avoid redundant #define untill we really need it!
	// {	MT7621_GDM1_TX_GBCNT	,"MT7621_GDM1_TX_GBCNT"		},
	// {	MT7621_FE_RST_GL		,"MT7621_FE_RST_GL"			},
	// {	MT7620_FE_INT_STATUS2	,"MT7620_FE_INT_STATUS2"	},
	{0, NULL}};

static void get_reg_name(u16 reg, char *nam)
{
	int i;
	const struct reg_2_nam *it;

	for (i = 0;; i++)
	{

		it = &reg_2_nam[i];

		if (it->nam == NULL)
			break;

		if (it->reg == reg)
		{
			strcpy(nam, it->nam);
			sprintf(nam, "%s (%04X)", it->nam, reg);
			return;
		}
	}

	sprintf(nam, "? (%04X)", reg);
}

static void mtk_netmap_log_reg_w32(u32 val, u16 reg)
{
	char nam[128];
	memset(nam, 0, 128);
	get_reg_name(reg, nam);

	nm_prinf("[mtk_netmap_log_reg_w32] %s put %08X\n", nam, val);
}

#endif // NOT_USED
