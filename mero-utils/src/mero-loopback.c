#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <poll.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <getopt.h>
#include <linux/if_tun.h>
#include <errno.h>
#include <time.h>
#include <sys/timerfd.h>

#define NETMAP_WITH_LIBS
#include <net/netmap_user.h>

// cpied stuff from mero-switch
#include "utils.h"
#include "eth_utils.h"

// nice and short type names
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

static int do_abort = 0;
struct nm_desc *cpu_port = NULL;

//
// struct for global application data
//

struct data_s
{
	char ifname[64]; // -i
	u16 vid1;		 // -a
	u16 vid2;		 // -b
	u8 opt_nozc;	 // -c

	u32 pktcounter;
};

static struct data_s data;

static void close_port()
{
	if (cpu_port)
	{
		nm_close(cpu_port);
		cpu_port = NULL;
	}
}

static int open_port()
{
	char netmap_name[64];

	sprintf(netmap_name, "netmap:%s@1", data.ifname);

	cpu_port = nm_open(netmap_name, NULL, NETMAP_NO_TX_POLL, NULL);

	if (!cpu_port)
	{
		close_port();
		D("nm_open %s failed", netmap_name);
		return (-1);
	}

	D("%s opened", netmap_name);

	return (0);
}

static void sigint_h(int sig)
{
	(void)sig; /* UNUSED */
	do_abort = 1;
	signal(SIGINT, SIG_DFL);
}

static inline u16 rd16(u8 *buf)
{
	u16 val;
	memcpy(&val, buf, 2);
	return (ntohs(val));
}

static inline u32 rd32(u8 *buf)
{
	u32 val;
	memcpy(&val, buf, 4);
	return (ntohl(val));
}

static inline void wr16(char *buf, u16 val)
{
	val = htons(val);
	memcpy(buf, &val, 2);
}

static inline char *binary(u32 val)
{
	static char str[33]; // 32 bit digits + terminating null

	for (int i = 0; i < 32; i++)
		str[31 - i] = ((val >> i) & 0x1) + '0';

	str[32] = '\0';
	return (str);
}

static void input_cpu()
{
	struct netmap_ring *rxring, *txring;
	struct netmap_slot *rxslot, *txslot;
	u8 *rxbuf, *txbuf;
	u16 len;
	u32 buf_idx;

	rxring = NETMAP_RXRING(cpu_port->nifp, 0);
	txring = NETMAP_TXRING(cpu_port->nifp, 0);

	if (nm_ring_empty(rxring))
		return;

	/* process until there are no incoming packets left */
	while (!nm_ring_empty(rxring))
	{
		rxslot = &rxring->slot[rxring->cur];
		rxbuf = (u8 *)NETMAP_BUF(rxring, rxslot->buf_idx);

		txslot = &txring->slot[txring->cur];
		txbuf = (u8 *)NETMAP_BUF(txring, txslot->buf_idx);

		/* expect tagged frames */
		if (ETH_IS8021Q(rxbuf))
		{
			// swap vids

			if (ETH_VLANID(rxbuf) == data.vid1)
			{
				wr16(rxbuf + ETH_HLEN, data.vid2);
			}
			else
			{
				wr16(rxbuf + ETH_HLEN, data.vid1);
			}

			// zerocopy vs memcpy

			if (data.opt_nozc)
			{
				memcpy(txbuf, rxbuf, rxslot->len);
				txslot->len = rxslot->len;
			}
			else
			{
				len = txslot->len;
				txslot->len = rxslot->len;
				rxslot->len = len;

				buf_idx = txslot->buf_idx;
				txslot->buf_idx = rxslot->buf_idx;
				rxslot->buf_idx = buf_idx;

				txslot->flags |= NS_BUF_CHANGED;
				rxslot->flags |= NS_BUF_CHANGED;
			}

			txring->head = txring->cur = nm_ring_next(txring, txring->cur);
		}

		data.pktcounter++;
		rxring->head = rxring->cur = nm_ring_next(rxring, rxring->cur);
	}
}

#ifdef UNUSED
static int tx_pending(struct nm_desc *port)
{
	struct netmap_ring *txring;

	for (u_int i = port->first_tx_ring; i <= port->last_tx_ring; i++)
	{
		txring = NETMAP_TXRING(port->nifp, i);

		if (nm_tx_pending(txring))
			return (1);
	}

	return (0);
}
#endif // UNUSED

static void usage()
{
	fprintf(stderr, "built %s %s\n\n", __DATE__, __TIME__);

	fprintf(stderr, "-i <ifname> : interface name.\n");
	fprintf(stderr, "-a <vlanid> : 1st vlanid\n");
	fprintf(stderr, "-b <vlanid> : 2nd vlanid\n");
	fprintf(stderr, "-c          : don't use zerocopy\n");

	exit(0);
}

static int parse_opts(int argc, char **argv)
{
	int ch;

	while ((ch = getopt(argc, argv, "a:b:chi:")) != -1)
	{
		switch (ch)
		{
		case 'a':
			data.vid1 = atoi(optarg);
			break;
		case 'b':
			data.vid2 = atoi(optarg);
			break;
		case 'c':
			data.opt_nozc = 1;
			break;
		case 'i':
			strcpy(data.ifname, optarg);
			break;
		default:
			D("bad option %c %s", ch, optarg);
			// fallthrough
		case 'h':
			usage();
			break;
		}
	}

	if (strlen(data.ifname) == 0)
	{
		fprintf(stderr, "missing interface name (-i)\n");
		return (-1);
	}

	if (data.vid1 == 0 || data.vid2 == 0)
	{
		fprintf(stderr, "missing vlan id (-a and -b)\n");
		return (-1);
	}

	if (data.opt_nozc)
	{
		fprintf(stderr, "zerocopy is deactivated!\n");
	}

	return (0);
}

int main(int argc, char **argv)
{
#define POLLLEN 1

	//
	// indexes of pollfd:
	// 0 -> cpu netmap port
	//

	struct pollfd pollfd[POLLLEN];

	memset(&data, 0, sizeof(data));

	if (parse_opts(argc, argv) < 0)
	{
		return (EXIT_FAILURE);
	}

	argc -= optind;
	argv += optind;

	if (open_port(0) < 0)
	{
		return (EXIT_FAILURE);
	}

	//
	// add netmap port to pollfd
	//

	pollfd[0].fd = cpu_port->fd;
	pollfd[0].events = POLLIN;

	D("ready: name %s rxrings %d memid %d", cpu_port->req.nr_name, cpu_port->req.nr_rx_rings, cpu_port->req.nr_arg2);

	signal(SIGINT, sigint_h); /* install signal handler to quit program. */

	while (!do_abort)
	{
		int ret = poll(pollfd, POLLLEN, 1000);

		if (ret < 0) /* error */
		{
			D("poll failed: %s (%d)", strerror(errno), errno);
		}

		if (pollfd[0].revents & POLLERR) /* handle netmap port -> index 0  */
		{
			D("error on [%s]", cpu_port->req.nr_name);
			return (EXIT_FAILURE);
		}

		if (pollfd[0].revents & POLLIN)
		{
			input_cpu();
		}

		pollfd[0].revents = 0;

		if (tx_pending(cpu_port))
		{
			ioctl(cpu_port->fd, NIOCTXSYNC);
		}
	}

	fprintf(stderr, "%d packets processed.\n", data.pktcounter);

	close_port();

	return (EXIT_SUCCESS);
}
