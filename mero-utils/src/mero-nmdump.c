#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <poll.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <getopt.h>
#include <linux/if_tun.h>
#include <errno.h>
#include <time.h>
#include <sys/timerfd.h>

#define NETMAP_WITH_LIBS
#include <net/netmap_user.h>

// nice and short type names
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

static char g_ifname[64] = {'\0'}; // -i
static u8 g_offset = 0;			   // -a

static u32 g_last_counter = 0;
static int do_abort = 0;
struct nm_desc *cpu_port = NULL;

//
// struct for global application data
//

struct data_s
{
	u8 tagged; // -b

	u32 pktcounter; // count all processd packets.
};

static struct data_s data;

static void close_port()
{
	if (cpu_port)
	{
		nm_close(cpu_port);
		cpu_port = NULL;
	}
}

static int open_port()
{
	char netmap_name[64];

	sprintf(netmap_name, "netmap:%s@1", g_ifname);

	cpu_port = nm_open(netmap_name, NULL, NETMAP_NO_TX_POLL, NULL);

	if (!cpu_port)
	{
		close_port();
		D("nm_open %s failed", netmap_name);
		return (-1);
	}

	D("%s opened", netmap_name);

	return (0);
}

static void sigint_h(int sig)
{
	(void)sig; /* UNUSED */
	do_abort = 1;
	signal(SIGINT, SIG_DFL);
}

static inline u16 rd16(u8 *buf)
{
	u16 val;
	memcpy(&val, buf, 2);
	return (ntohs(val));
}

static inline u32 rd32(u8 *buf)
{
	u32 val;
	memcpy(&val, buf, 4);
	return (ntohl(val));
}

static inline char *binary(u32 val)
{
	static char str[33]; // 32 bit digits + terminating null

	for (int i = 0; i < 32; i++)
		str[31 - i] = ((val >> i) & 0x1) + '0';

	str[32] = '\0';
	return (str);
}

static void input_cpu()
{
	struct nm_desc *rxport = cpu_port;

	char hexdump[1024], *tmp;

	for (u_int i = rxport->first_rx_ring; i <= rxport->last_rx_ring; i++)
	{
		struct netmap_ring *rxring = NETMAP_RXRING(rxport->nifp, i);

		if (nm_ring_empty(rxring))
			continue;

		/* process until there are no incoming packets left */
		while (!nm_ring_empty(rxring))
		{
			struct netmap_slot *rxslot = &rxring->slot[rxring->cur];
			u8 *rxbuf = (u8 *)NETMAP_BUF(rxring, rxslot->buf_idx);

			u8 offs = g_offset ? 2 : 0;
			offs += data.tagged ? 4 : 0;

			u32 counter = rd32(rxbuf + 42 + offs); // read counter from pkt-gen packet
			if (counter - g_last_counter > 1)
			{
				fprintf(stdout, "OUT OF ORDER: current %08X (%d) previous %08X (%d)\n", counter, counter, g_last_counter, g_last_counter);
				fprintf(stderr, "OUT OF ORDER: current %08X (%d) previous %08X (%d)\n", counter, counter, g_last_counter, g_last_counter);
			}
			tmp = hexdump;
			for (int i = 0; i < 50; i++)
				tmp += sprintf(tmp, "%02X", rxbuf[i]);
			*tmp = '\0';
			printf("%5d  %s  %s\n", counter, binary(counter), hexdump);

			g_last_counter = counter;

			data.pktcounter++;

			rxring->head = rxring->cur = nm_ring_next(rxring, rxring->cur); // return processed slot to kernel
		}
	}
}

static int tx_pending(struct nm_desc *port)
{
	struct netmap_ring *txring;

	for (u_int i = port->first_tx_ring; i <= port->last_tx_ring; i++)
	{
		txring = NETMAP_TXRING(port->nifp, i);

		if (nm_tx_pending(txring))
			return (1);
	}

	return (0);
}

static void usage()
{
	fprintf(stderr, "built %s %s\n\n", __DATE__, __TIME__);

	fprintf(stderr, "-i <ifname> : interface name\n");
	fprintf(stderr, "-a          : if feature RX_2B_OFFSET is active\n");
	fprintf(stderr, "-b          : if VLAN tags are used\n");

	exit(0);
}

static int parse_opts(int argc, char **argv)
{
	int ch;

	while ((ch = getopt(argc, argv, "abhi:")) != -1)
	{
		switch (ch)
		{
		case 'a':
			g_offset = 1;
			break;
		case 'b':
			data.tagged = 1;
			break;
		case 'i':
			strcpy(g_ifname, optarg);
			break;
		default:
			D("bad option %c %s", ch, optarg);
			// fallthrough
		case 'h':
			usage();
			break;
		}
	}

	if (strlen(g_ifname) == 0)
	{
		fprintf(stderr, "missing interface name (-i)\n");
		return (-1);
	}

	return (0);
}

int main(int argc, char **argv)
{
#define POLLLEN 1

	//
	// indexes of pollfd:
	// 0 -> cpu netmap port
	//

	struct pollfd pollfd[POLLLEN];

	memset(&data, 0, sizeof(data));

	if (parse_opts(argc, argv) < 0)
	{
		return (EXIT_FAILURE);
	}

	argc -= optind;
	argv += optind;

	if (open_port(0) < 0)
	{
		return (EXIT_FAILURE);
	}

	//
	// add netmap port to pollfd
	//

	pollfd[0].fd = cpu_port->fd;
	pollfd[0].events = POLLIN;

	D("ready: name %s rxrings %d memid %d", cpu_port->req.nr_name, cpu_port->req.nr_rx_rings, cpu_port->req.nr_arg2);

	signal(SIGINT, sigint_h); /* install signal handler to quit program. */

	while (!do_abort)
	{
		int ret = poll(pollfd, POLLLEN, 1000);

		if (ret < 0) /* error */
		{
			D("poll failed: %s (%d)", strerror(errno), errno);
		}

		if (pollfd[0].revents & POLLERR) /* handle netmap port -> index 0  */
		{
			D("error on [%s]", cpu_port->req.nr_name);
			return (EXIT_FAILURE);
		}

		if (pollfd[0].revents & POLLIN)
		{
			input_cpu();
		}

		pollfd[0].revents = 0;

		if (tx_pending(cpu_port))
		{
			ioctl(cpu_port->fd, NIOCTXSYNC);
		}
	}

	fprintf(stderr, "%d packets processed.\n", data.pktcounter);

	close_port();

	return (EXIT_SUCCESS);
}
