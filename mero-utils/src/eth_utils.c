#include "eth_utils.h"

#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>

#include "netmap.h"
#include "utils.h"

/*******************************************************************************
 ******************************************************************************/
char *eth_addr_to_str(const struct etha_t *addr, char *str)
{
    static char sstr[18 + 1] = {0};

    if (str)
    {
        sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x", addr->addr[0], addr->addr[1], addr->addr[2], addr->addr[3], addr->addr[4], addr->addr[5]);
        return (NULL);
    }

    sprintf(sstr, "%02x:%02x:%02x:%02x:%02x:%02x", addr->addr[0], addr->addr[1], addr->addr[2], addr->addr[3], addr->addr[4], addr->addr[5]);
    return (sstr);
}

/*******************************************************************************
 * https://stackoverflow.com/a/20553913
 ******************************************************************************/
int eth_str_to_addr(const char *str, struct etha_t *mac)
{
    int values[6], rv;

    char dummy; /* to avoid warning at sscanf() */

    /* 
	 * Because of %c at the end of format string we will get return value != 6 if there is just one more char in the expected mac address string.
	 */

    if ((rv = sscanf(str, "%x:%x:%x:%x:%x:%x%c", &values[0], &values[1], &values[2], &values[3], &values[4], &values[5], &dummy)) == 6)
    {
        /* convert to uint8_t */
        for (int i = 0; i < 6; ++i)
        {
            mac->addr[i] = (uint8_t)values[i];
        }
    }
    else
    {

        D("Invalid MAC '%s' (rv=%d).", str, rv);

        return (-1);
    }

    return (0);
}

/*******************************************************************************
 ******************************************************************************/
void eth_set_dst_addr(const struct etha_t *addr, char *buf)
{
    memcpy(buf, addr->addr, ETH_ALEN);
}

/*******************************************************************************
 ******************************************************************************/
void eth_set_src_addr(const struct etha_t *addr, char *buf)
{
    memcpy(buf + ETH_ALEN, addr->addr, ETH_ALEN);
}

/*******************************************************************************
 ******************************************************************************/
void eth_get_src_addr(struct etha_t *addr, const char *buf)
{
    memcpy(addr->addr, buf + ETH_ALEN, ETH_ALEN);
}

/*******************************************************************************
 ******************************************************************************/
void eth_create_tagged(struct netmap_slot *slot, char *buf, const struct etha_t *dst, const struct etha_t *src, uint16_t vlanid, uint16_t ethertype)
{
    eth_set_dst_addr(dst, buf);
    eth_set_src_addr(src, buf);
    SUINT16(buf, VLAN_TPID, ETH_P_8021Q);
    SUINT16(buf, VLAN_TCI, vlanid);
    SUINT16(buf, VLAN_TYPE, ethertype);

    slot->len = VLAN_HLEN;
}
