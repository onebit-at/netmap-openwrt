/*
 * 
 */

#include "udp_utils.h"

#include "ip_utils.h"

/*******************************************************************************
 * 
 ******************************************************************************/
char *udp_create_header(struct netmap_slot *slot, char *buf, char *udp_header, uint16_t source_port, uint16_t destination_port)
{
    /* udp_header is used to iterate over bytes and points to udp-payload at the end of the function */

    /* udp_header -> source port */

    SUINT16(udp_header, 0, source_port);
    udp_header += 2;

    /* udp_header -> destination port */

    SUINT16(udp_header, 0, destination_port);
    udp_header += 2;

    /* udp_header -> length ... filled via udp_finish_header() */

    SUINT16(udp_header, 0, 0);
    udp_header += 2;

    /* udp_header -> checksum ... filled via udp_finish_header() */

    SUINT16(udp_header, 0, 0);
    udp_header += 2;

    return (udp_header);
}

/*******************************************************************************
 * 
 ******************************************************************************/
void udp_finish_header(struct netmap_slot *slot, char *buf, char *udp_header, char *udp_payload, const char *ip_header)
{
    uint16_t length = slot->len - (udp_header - buf);
    SUINT16(udp_header, 4, length);

    char pseudo_header[12];

    memcpy(pseudo_header, ip_header + 12, 4);     // copy source ip address
    memcpy(pseudo_header + 4, ip_header + 16, 4); // copy destination ip address
    pseudo_header[8] = 0; // reserved
    pseudo_header[9] = ip_header[9]; // copy protocol
    SUINT16(pseudo_header, 10, length);

    // checksum over pseudo header, udp header and udp payload.
    uint16_t udp_checksum = checksum(pseudo_header, 12, udp_header, length);
    SUINT16(udp_header, 6, udp_checksum);
}