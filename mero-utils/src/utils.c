/*
 *
 */

#include "utils.h"

#include <net/if.h>
#include <net/if_arp.h>
#include <linux/if_ether.h>

#include "sysfs_utils.h"

/*******************************************************************************
 ******************************************************************************/
int check_interface_state(const char *ifname)
{
    uint32_t flags = sysfs_get_if_flags(ifname);
    int carrier = sysfs_get_if_carrier(ifname);

    if (!(flags & IFF_UP))
    {
        D("ERROR: %s not UP", ifname);
        return (0);
    }

    if (!(flags & IFF_PROMISC))
    {
        D("ERROR: %s not PROMISC", ifname);
        return (0);
    }

    if (!carrier)
    {
        D("ERROR: %s not CARRIER", ifname);
        return (0);
    }

    return (1);
}

/*******************************************************************************
 ******************************************************************************/
void lbtrim(char *str)
{
    str[strcspn(str, "\r\n")] = '\0';
}

/*******************************************************************************
 ******************************************************************************/
int tx_pending(struct nm_desc *port)
{
    struct netmap_ring *txring;

    for (u_int i = port->first_tx_ring; i <= port->last_tx_ring; i++)
    {
        txring = NETMAP_TXRING(port->nifp, i);

        if (nm_tx_pending(txring))
            return (1);
    }

    return (0);
}
