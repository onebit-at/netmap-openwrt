/*
 * useful stuff for standard ipv4 packets.
 */

#pragma once

#include <stdint.h>
#include <netinet/in.h>

#include "utils.h"

/* number of bytes used by ipv4 address */
#define IP4_ALEN 4

/* get payload protocol */
#define IP4_PROTO(ip_packet) (UINT8(ip_packet, 9))

/* get length of header */
#define IP4_HLEN(ip_packet) ((UINT8(ip_packet, 0) & 0x0F) * 4)

/*******************************************************************************
 ******************************************************************************/
struct in_addr ip_get_src_addr(const char *ip_packet);

/*******************************************************************************
 ******************************************************************************/
struct in_addr ip_get_dst_addr(const char *ip_packet);

/*******************************************************************************
 ******************************************************************************/
char *ip_addr_to_str(struct in_addr *addr, char *str);

/*******************************************************************************
 ******************************************************************************/
int ip_str_to_addr(const char *str, struct in_addr *addr);

/*******************************************************************************
 ******************************************************************************/
int ip_is_dhcp(char *ip_packet);

/*******************************************************************************
 * Generic checksum calculation used by IP and TCP.
 ******************************************************************************/
uint16_t checksum(const char *header, uint16_t header_length, const char *data, uint16_t data_length);

/*******************************************************************************
 * 
 ******************************************************************************/
char *ip_create_header(struct netmap_slot *slot, char *buf, char *ip_header, const struct in_addr *source, const struct in_addr *destination, uint8_t protocol);

/*******************************************************************************
 * 
 ******************************************************************************/
void ip_finish_header(struct netmap_slot *slot, char *buf, char *ip_header, char *ip_payload);
