/*
 * general helper stuff.
 */

#pragma once

#include <arpa/inet.h> /* htons, ... */

#include "netmap.h"

/* get uint16_t in host byte-order at byte offset */
#define UINT16H(buf, off) (ntohs(*((uint16_t *)(buf + (off)))))

/* get uint8_t at byte offset */
#define UINT8(buf, off) (*((uint8_t *)(buf + (off))))

/* set uint16_t in network byte-order at byte offset */
#define SUINT16(buf, off, val) (*((uint16_t *)(buf + (off))) = htons(val))

/* set uint8_t at byte offset */
#define SUINT8(buf, off, val) (*((uint8_t *)(buf + (off))) = (val))

/* return() if there is no space for sending in tx-ring */
#define DROP_IF_FULLTX(p, r)                           \
    if (nm_ring_empty(r))                              \
    {                                                  \
        /* D("%s: no space in tx", p->req.nr_name); */ \
        return;                                        \
    }

/*******************************************************************************
 ******************************************************************************/
int check_interface_state(const char *ifname);

/*******************************************************************************
 * Removing trailing newline character from null-terminated string.
 * https://stackoverflow.com/questions/2693776/removing-trailing-newline-character-from-fgets-input/28462221#28462221
 ******************************************************************************/
void lbtrim(char *str);

/*******************************************************************************
 * Checks if there is a pending TX packet on this port.
 * Considers all TX rings on @port.
 ******************************************************************************/
int tx_pending(struct nm_desc *port);