/*
 * useful stuff handling ethernet frames.
 */

#pragma once

#include <stdint.h>
#include <linux/if_ether.h>

#include "netmap.h"

/* header length of 802.1Q VLAN ethernet frame. basically that's ETH_HLEN + 4.
 * there is VLAN_HLEN in host system in e.g. /usr/src/linux-headers-4.8.0-56/include/linux/if_vlan.h but not in lede so here is our own constant. */
#define VLAN_HLEN 18

/* offsets for individual fields of 802.1Q vlan ethernet frame. */

#define VLAN_TPID (ETH_ALEN * 2)     /* tag protocol id, always 0x8100 */
#define VLAN_TCI (ETH_ALEN * 2 + 2)  /* tag control information a.k.a. vlan id */
#define VLAN_TYPE (ETH_ALEN * 2 + 4) /* ethertype */

/* minimum string length to hold string representation of a mac address plus \0 at the end.
 * mac address: xx:xx:xx:xx:xx:xx */
#define ETH_ALEN_STR 18

/* generic check for ethertype */
#define ETH_ISTYPE(buf, type) (UINT16H(buf, ETH_ALEN * 2) == type)

/* specific check for 802.1Q */
#define ETH_IS8021Q(buf) (ETH_ISTYPE(buf, ETH_P_8021Q))

/* get vlanid of hopefully 802.1Q ethernet frame */
#define ETH_VLANID(buf) (UINT16H(buf, ETH_HLEN))

struct etha_t
{
    uint8_t addr[ETH_ALEN];
};

/*******************************************************************************
 ******************************************************************************/
char *eth_addr_to_str(const struct etha_t *addr, char *str);

/*******************************************************************************
 ******************************************************************************/
int eth_str_to_addr(const char *str, struct etha_t *mac);

/*******************************************************************************
 ******************************************************************************/
void eth_set_dst_addr(const struct etha_t *addr, char *buf);

/*******************************************************************************
 ******************************************************************************/
void eth_set_src_addr(const struct etha_t *addr, char *buf);

/*******************************************************************************
 ******************************************************************************/
void eth_get_src_addr(struct etha_t *addr, const char *buf);

/*******************************************************************************
 ******************************************************************************/
void eth_create_tagged(struct netmap_slot *slot, char *buf, const struct etha_t *dst, const struct etha_t *src, uint16_t vlanid, uint16_t ethertype);
