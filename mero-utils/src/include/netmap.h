/*
 * sum of all includes for netmap.
 */

#pragma once

#include <sys/time.h>
#include <sys/types.h>
#define NETMAP_WITH_LIBS
#include <net/netmap_user.h>

/* value for struct netmap_ring::dir. note, comments in netmap.h are wrong!!! */
#define DIR_RX 0
#define DIR_TX 1
