/*
 *
 */

#pragma once

#include "utils.h"

/* get destination port */
#define UDP_DSTPORT(udp) (UINT16H(udp, 2))

/*******************************************************************************
 * 
 ******************************************************************************/
char *udp_create_header(struct netmap_slot *slot, char *buf, char *udp_header, uint16_t source_port, uint16_t destination_port);

/*******************************************************************************
 * 
 ******************************************************************************/
void udp_finish_header(struct netmap_slot *slot, char *buf, char *udp_header, char *udp_payload, const char *ip_header);
