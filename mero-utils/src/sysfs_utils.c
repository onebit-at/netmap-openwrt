/*
 *
 */

#include "sysfs_utils.h"

#include <limits.h>
#include <stdio.h>

/*******************************************************************************
 ******************************************************************************/
uint32_t sysfs_get_if_flags(const char *ifname)
{
    char filename[PATH_MAX + 1];
    sprintf(filename, "/sys/class/net/%s/flags", ifname);

    FILE *file = fopen(filename, "r");

    uint32_t flags = -1; /* max unsigned value */

    if (file)
    {
        fscanf(file, "%x", &flags);
        fclose(file);
    }

    return (flags);
}

/*******************************************************************************
 ******************************************************************************/
int sysfs_get_if_carrier(const char *ifname)
{
    char filename[PATH_MAX + 1];
    sprintf(filename, "/sys/class/net/%s/carrier", ifname);

    FILE *file = fopen(filename, "r");

    uint32_t flag = 0;

    if (file)
    {
        fscanf(file, "%d", &flag);
        fclose(file);
    }

    return (flag);
}
