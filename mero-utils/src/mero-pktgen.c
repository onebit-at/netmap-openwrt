#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <poll.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <getopt.h>
#include <linux/if_tun.h>
#include <errno.h>
#include <time.h>
#include <sys/timerfd.h>

#define NETMAP_WITH_LIBS
#include <net/netmap_user.h>

// cpied stuff from mero-switch
#include "utils.h"
#include "eth_utils.h"
#include "ip_utils.h"
#include "udp_utils.h"

// nice and short type names
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

int delay = 10;			  // -d
char ifname[64] = {'\0'}; // -i
u16 g_pktlen = 100;		  // -l
u16 rate = 1;			  // -r
u16 g_pktleninc = 0;	  // -a

static u16 g_curpktlen; // current packet length starts with packet length
static int do_abort = 0;
struct nm_desc *cpu_port = NULL;

//
// struct for global application data
//

struct data_s
{
	// program options

	u8 opt_ethertype;	 // see option -b
	u8 opt_udpports;	  // see option -c
	u8 opt_ipaddrs;		  // see option -e
	u8 opt_zeroudpchksum; // see option -f
	u8 opt_ipproto;		  // see option -g
	u8 opt_macaddrs;	  // see option -j

	// runtime stuff

	u32 counter; // packet counter
};

static struct data_s data;

static void close_port()
{
	if (cpu_port)
	{
		nm_close(cpu_port);
		cpu_port = NULL;
	}
}

static int open_port()
{
	char netmap_name[64];

	sprintf(netmap_name, "netmap:%s@1", ifname);

	cpu_port = nm_open(netmap_name, NULL, NETMAP_NO_TX_POLL, NULL);

	if (!cpu_port)
	{
		close_port();
		D("nm_open %s failed", netmap_name);
		return (-1);
	}

	D("%s opened", netmap_name);

	return (0);
}

static void sigint_h(int sig)
{
	(void)sig; /* UNUSED */
	do_abort = 1;
	signal(SIGINT, SIG_DFL);
}

static void input_cpu()
{
	struct nm_desc *rxport = cpu_port;
	struct netmap_ring *rxring;
	//struct netmap_slot *rxslot;
	//char *rxbuf;

	for (u_int i = rxport->first_rx_ring; i <= rxport->last_rx_ring; i++)
	{
		rxring = NETMAP_RXRING(rxport->nifp, i);

		/* skip processing if there are no received packets */
		if (nm_ring_empty(rxring))
			continue;

		/* process until there are no incoming packets left */
		while (!nm_ring_empty(rxring))
		{
			//rxslot = &rxring->slot[rxring->cur];
			//rxbuf = NETMAP_BUF(rxring, rxslot->buf_idx);

			/* return processed slot to kernel */
			rxring->head = rxring->cur = nm_ring_next(rxring, rxring->cur);
		}
	}
}

#ifdef UNUSED
static int tx_pending(struct nm_desc *port)
{
	struct netmap_ring *txring;

	for (u_int i = port->first_tx_ring; i <= port->last_tx_ring; i++)
	{
		txring = NETMAP_TXRING(port->nifp, i);

		if (nm_tx_pending(txring))
			return (1);
	}

	return (0);
}
#endif // UNUSED

#ifdef UNUSED
static void fill_payload(char *bytes, u16 len, u16 val)
{
	for (u16 i = 0; i < len - 14; i++)
	{
		// even : high byte (zero is even)
		// odd : low byte of value
		// results is hi,lo,hi,lo,hi,lo,hi,lo,...
		// i & 1 is true if i is odd
		u8 hilo = (i & 1) ? val & 0xff : (val >> 8) & 0xff;
		*(bytes + i) = hilo;
	}
}
#endif // UNUSED

//
// fills len bytes with random value starting from bytes.
//
static void fill_payload_random(char *bytes, u16 len)
{
	for (u16 i = 0; i < len; i++)
	{
		*bytes = rand() % 256;
		bytes++;
	}
}

//
// copied and modified eth_create_tagged()
//
static void eth_create_untagged(struct netmap_slot *slot, char *buf, const struct etha_t *dst, const struct etha_t *src, uint16_t ethertype)
{
	eth_set_dst_addr(dst, buf);
	eth_set_src_addr(src, buf);
	SUINT16(buf, 12, ethertype);
	slot->len = ETH_HLEN;
}

static inline void wr16(char *buf, u16 val)
{
	val = htons(val);
	memcpy(buf, &val, 2);
}

static inline void wr32(char *buf, u32 val)
{
	val = htonl(val);
	memcpy(buf, &val, 4);
}

static void create_pkt(struct netmap_slot *txslot, char *txbuf)
{
	// Create ETH

	struct etha_t dst_mac = {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}}; // broadcast
	struct etha_t src_mac = {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

	if (data.opt_macaddrs)
	{
		dst_mac.addr[0] = 0x00;
		dst_mac.addr[1] = 0xAA;
		dst_mac.addr[2] = (data.counter >> 24) & 0xFF;
		dst_mac.addr[3] = (data.counter >> 16) & 0xFF;
		dst_mac.addr[4] = (data.counter >> 8) & 0xFF;
		dst_mac.addr[5] = data.counter & 0xFF;

		src_mac.addr[0] = 0x00;
		src_mac.addr[1] = 0x11;
		src_mac.addr[2] = (data.counter >> 24) & 0xFF;
		src_mac.addr[3] = (data.counter >> 16) & 0xFF;
		src_mac.addr[4] = (data.counter >> 8) & 0xFF;
		src_mac.addr[5] = data.counter & 0xFF;
	}

	u16 ethertype = ETH_P_IP;

	if (data.opt_ethertype)
	{
		ethertype = data.counter; // use two low-order bytes of counter as ethertype
	}

	eth_create_untagged(txslot, txbuf, &dst_mac, &src_mac, ethertype);

	//
	// Create IP
	//
	// * addresses copied from pkt-gen.
	//

	struct in_addr source_ip;
	ip_str_to_addr("10.0.0.1", &source_ip);

	struct in_addr destination_ip;
	ip_str_to_addr("10.1.0.1", &destination_ip);

	if (data.opt_ipaddrs)
	{
		ip_str_to_addr("10.0.0.0", &source_ip);
		ip_str_to_addr("11.0.0.0", &destination_ip);

		source_ip.s_addr |= htonl(data.counter & 0x00FFFFFF);
		destination_ip.s_addr |= htonl(data.counter & 0x00FFFFFF);
	}

	char *ip_header = txbuf + txslot->len;
	char *ip_payload = ip_create_header(txslot, txbuf, ip_header, &source_ip, &destination_ip, IPPROTO_UDP);

	if (data.opt_ipproto)
	{
		*(ip_header + 9) = data.counter;
	}

	//
	// Create UDP
	//
	// * port numbers copied from pkt-gen.
	//

	u16 src_port, dst_port;
	src_port = dst_port = 1234;

	if (data.opt_udpports)
	{
		src_port = dst_port = data.counter; // use two low-order bytes of counter as port-numbers
	}

	char *udp_header = ip_payload;
	char *udp_payload = udp_create_header(txslot, txbuf, udp_header, src_port, dst_port);

	wr32(udp_payload, data.counter);
	udp_payload += 4;

	txslot->len = udp_payload - txbuf; // update txslot->len

	u16 payload_len = g_pktlen - txslot->len;
	fill_payload_random(udp_payload, payload_len);
	txslot->len += payload_len;

	// Finish UDP

	udp_finish_header(txslot, txbuf, udp_header, udp_payload, ip_header);

	if (data.opt_zeroudpchksum)
	{
		wr16(udp_header + 6, 0);
	}

	// Finish IP

	ip_finish_header(txslot, txbuf, ip_header, ip_payload);
}

static void send_pkt()
{
	struct netmap_ring *txring = NETMAP_TXRING(cpu_port->nifp, cpu_port->first_tx_ring);
	struct netmap_slot *txslot;
	char *txbuf;

	for (u16 r = 0; r < rate; r++)
	{
		txslot = &txring->slot[txring->cur];
		txbuf = NETMAP_BUF(txring, txslot->buf_idx);

		create_pkt(txslot, txbuf);

		if ((g_curpktlen += g_pktleninc) > ETH_FRAME_LEN)
			g_curpktlen = g_pktlen;

		data.counter++;
		txring->head = txring->cur = nm_ring_next(txring, txring->cur);
	}

	D("sent %d packets in %d msecs, total sent %d.", rate, delay, data.counter);
}

static void usage()
{
	fprintf(stderr, "built %s %s\n\n", __DATE__, __TIME__);

	fprintf(stderr, "-i <ifname> : interface name.\n");
	fprintf(stderr, "-a <inc>    : packet length increment (default: 0)\n");
	fprintf(stderr, "-b          : fill ethertype with packet counter\n");
	fprintf(stderr, "-c          : fill udp port-numbers with packet counter\n");
	fprintf(stderr, "-d <delay>  : delay between packets in millisecs (default: 10)\n");
	fprintf(stderr, "-e          : fill ip addresses with packet counter\n");
	fprintf(stderr, "-f          : set udp checksum to zero\n");
	fprintf(stderr, "-g          : fill ip proto with packet counter\n");
	fprintf(stderr, "-h          : help\n");
	fprintf(stderr, "-j          : fill mac addresses with packet counter\n");
	fprintf(stderr, "-l <len>    : packet length including all header (default: 100)\n");
	fprintf(stderr, "-r <num>    : number of packets per interval (default: 1)\n");

	exit(0);
}

static int parse_opts(int argc, char **argv)
{
	int ch;

	while ((ch = getopt(argc, argv, "a:bcd:efghi:jl:r:")) != -1)
	{
		switch (ch)
		{
		case 'a':
			g_pktleninc = atoi(optarg);
			break;
		case 'b':
			data.opt_ethertype = 1;
			break;
		case 'c':
			data.opt_udpports = 1;
			break;
		case 'd':
			delay = atoi(optarg);
			break;
		case 'e':
			data.opt_ipaddrs = 1;
			break;
		case 'f':
			data.opt_zeroudpchksum = 1;
			break;
		case 'g':
			data.opt_ipproto = 1;
			break;
		case 'i':
			strcpy(ifname, optarg);
			break;
		case 'j':
			data.opt_macaddrs = 1;
			break;
		case 'l':
			g_pktlen = atoi(optarg);
			break;
		case 'r':
			rate = atoi(optarg);
			break;
		default:
			D("bad option %c %s", ch, optarg);
			// fallthrough
		case 'h':
			usage();
			break;
		}
	}

	if (g_pktleninc < 0)
	{
		fprintf(stderr, "invalid packet length increment %d (-a)\n", g_pktleninc);
		return (-1);
	}

	if (delay <= 0)
	{
		fprintf(stderr, "invalid delay %d (-d)\n", delay);
		return (-1);
	}

	if (strlen(ifname) == 0)
	{
		fprintf(stderr, "missing interface name (-i)\n");
		return (-1);
	}

	if (g_pktlen < ETH_ZLEN)
	{
		fprintf(stderr, "packet length %d is too small. minimum is %d.\n", g_pktlen, ETH_ZLEN);
		return (-1);
	}

	if (g_pktlen > ETH_FRAME_LEN)
	{
		fprintf(stderr, "packet length %d is to big. maximum is %d.\n", g_pktlen, ETH_FRAME_LEN);
		return (-1);
	}

	g_curpktlen = g_pktlen;

	if (rate <= 0 || rate > 500)
	{
		fprintf(stderr, "invalid rate %d (-r)\n", rate);
		return (-1);
	}

	return (0);
}

static void init_random()
{
	struct timeval t1;
	gettimeofday(&t1, NULL);
	srand(t1.tv_usec * t1.tv_sec);
}

int main(int argc, char **argv)
{
#define POLLLEN 1

	//
	// indexes of pollfd:
	// 0 -> cpu netmap port
	//

	struct pollfd pollfd[POLLLEN];

	memset(&data, 0, sizeof(data));

	init_random();

	if (parse_opts(argc, argv) < 0)
	{
		return (EXIT_FAILURE);
	}

	argc -= optind;
	argv += optind;

	if (open_port(0) < 0)
	{
		return (EXIT_FAILURE);
	}

	//
	// add netmap port to pollfd
	//

	pollfd[0].fd = cpu_port->fd;
	pollfd[0].events = POLLIN;

	D("ready: name %s rxrings %d memid %d", cpu_port->req.nr_name, cpu_port->req.nr_rx_rings, cpu_port->req.nr_arg2);

	signal(SIGINT, sigint_h); /* install signal handler to quit program. */

	D("wait a few seconds...");
	sleep(5);

	while (!do_abort)
	{
		int ret = poll(pollfd, POLLLEN, delay); // poll() timeout 'delay'

		if (ret < 0) /* error */
		{
			D("poll failed: %s (%d)", strerror(errno), errno);
		}

		if (pollfd[0].revents & POLLERR) /* handle netmap port -> index 0  */
		{
			D("error on [%s]", cpu_port->req.nr_name);
			return (EXIT_FAILURE);
		}

		if (pollfd[0].revents & POLLIN)
			input_cpu();

		pollfd[0].revents = 0;

		send_pkt();

		if (tx_pending(cpu_port))
		{
			ioctl(cpu_port->fd, NIOCTXSYNC);
		}
	}

	close_port();

	return (EXIT_SUCCESS);
}
