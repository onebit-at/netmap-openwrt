#include "ip_utils.h"

#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>

#include "udp_utils.h"

/*******************************************************************************
 ******************************************************************************/
struct in_addr ip_get_src_addr(const char *ip_packet)
{
    in_addr_t *p = (in_addr_t *)(ip_packet + 12);

    struct in_addr ret;
    ret.s_addr = *p;

    return ret;
}

/*******************************************************************************
 ******************************************************************************/
struct in_addr ip_get_dst_addr(const char *ip_packet)
{
    in_addr_t *p = (in_addr_t *)(ip_packet + 16);

    struct in_addr ret;
    ret.s_addr = *p;

    return ret;
}

/*******************************************************************************
 * addr.s_addr must be in network byte order!
 ******************************************************************************/
char *ip_addr_to_str(struct in_addr *addr, char *str)
{
    static char sstr[INET_ADDRSTRLEN + 1] = {0};

    inet_ntop(AF_INET, addr, sstr, INET_ADDRSTRLEN);

    if (str)
    {
        strcpy(str, sstr);
        return NULL;
    }

    return sstr;
}

/*******************************************************************************
 ******************************************************************************/
int ip_str_to_addr(const char *str, struct in_addr *addr)
{
    return (inet_pton(AF_INET, str, addr));
}

/*******************************************************************************
 ******************************************************************************/
int ip_is_dhcp(char *ip_packet)
{
    if (IP4_PROTO(ip_packet) == IPPROTO_UDP)
    {
        char *udp = ip_packet + IP4_HLEN(ip_packet); // udp payload

        if (UDP_DSTPORT(udp) == 67)
        {
            /* this is dhcp */

            return (1);
        }
    }

    return (0);
}

/*******************************************************************************
 ******************************************************************************/
uint16_t checksum(const char *header, uint16_t header_length, const char *data, uint16_t data_length)
{
    uint16_t i;

    // Initialise accumulator.
    uint32_t acc = 0xffff;

    // Handle optional header.
    // Memory of header and data are considered as seamlessly connected. Therefore
    // header is assumed to contain only full 16-bit blocks.
    if (header)
    {
        for (i = 0; i + 1 < header_length; i += 2)
        {
            uint16_t word;
            memcpy(&word, header + i, 2);
            acc += ntohs(word);
            if (acc > 0xffff)
            {
                acc -= 0xffff;
            }
        }
    }

    // Handle complete 16-bit blocks.
    for (i = 0; i + 1 < data_length; i += 2)
    {
        uint16_t word;
        memcpy(&word, data + i, 2);
        acc += ntohs(word);
        if (acc > 0xffff)
        {
            acc -= 0xffff;
        }
    }

    // Handle any partial block at the end of the data.
    if (data_length & 1)
    {
        uint16_t word = 0;
        memcpy(&word, data + data_length - 1, 1);
        acc += ntohs(word);
        if (acc > 0xffff)
        {
            acc -= 0xffff;
        }
    }

    return ~acc;
}

/*******************************************************************************
 *
 ******************************************************************************/
char *ip_create_header(struct netmap_slot *slot, char *buf, char *ip_header, const struct in_addr *source, const struct in_addr *destination, uint8_t protocol)
{
    /* ip_header is used to iterate over bytes and points to ip-payload at the end of the function */

    /* ip_header -> version & ihl ... filled via ip_finish_header() */

    SUINT8(ip_header, 0, 0);
    ip_header += 1;

    /* ip_header -> tos */

    SUINT8(ip_header, 0, 0x10);
    ip_header += 1;

    /* ip_header -> total length ... filled via ip_finish_header() */

    SUINT16(ip_header, 0, 0);
    ip_header += 2;

    /* ip_header -> identification */

    SUINT16(ip_header, 0, 0);
    ip_header += 2;

    /* ip_header -> flags & fragment offset */

    SUINT16(ip_header, 0, 0);
    ip_header += 2;

    /* ip_header -> ttl */

    SUINT8(ip_header, 0, 0x80);
    ip_header += 1;

    /* ip_header -> protocol */

    SUINT8(ip_header, 0, protocol);
    ip_header += 1;

    /* ip_header -> header checksum ... filled via ip_finish_header() */

    SUINT16(ip_header, 0, 0);
    ip_header += 2;

    /* ip_header -> source address */

    memcpy(ip_header, &source->s_addr, IP4_ALEN);
    ip_header += IP4_ALEN;

    /* ip_header -> destination address */

    memcpy(ip_header, &destination->s_addr, IP4_ALEN);
    ip_header += IP4_ALEN;

    /* ip_header -> options */

    return (ip_header);
}

/*******************************************************************************
 * 
 ******************************************************************************/
void ip_finish_header(struct netmap_slot *slot, char *buf, char *ip_header, char *ip_payload)
{
    uint16_t header_length = (ip_payload - ip_header);

    uint8_t version = 4; /* IPv4 */

    uint8_t ihl = (header_length * 8) / 32; /* size of header in 32bit units */

    uint8_t version_and_ihl = (version << 4) | (ihl & 0x0F);
    SUINT8(ip_header, 0, version_and_ihl);

    uint16_t total_length = slot->len - (ip_header - buf);
    SUINT16(ip_header, 2, total_length);

    uint16_t header_checksum = checksum(NULL, 0, ip_header, header_length);
    SUINT16(ip_header, 10, header_checksum);
}
