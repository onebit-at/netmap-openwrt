#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <asm/mach-ralink/ralink_regs.h>

#include "gsw_mt7620.h"

#define SYSC_REG_RSTCTRL        0x34

void fe_reset(u32 reset_bits)
{
        u32 t;

        t = rt_sysc_r32(SYSC_REG_RSTCTRL);
        t |= reset_bits;
        rt_sysc_w32(t, SYSC_REG_RSTCTRL);
        usleep_range(10, 20);

        t &= ~reset_bits;
        rt_sysc_w32(t, SYSC_REG_RSTCTRL);
        usleep_range(10, 20);
}
EXPORT_SYMBOL(fe_reset);

void mtk_switch_w32(struct mt7620_gsw *gsw, u32 val, unsigned reg)
{
        iowrite32(val, gsw->base + reg);
}
EXPORT_SYMBOL(mtk_switch_w32);

u32 mtk_switch_r32(struct mt7620_gsw *gsw, unsigned reg)
{
        return ioread32(gsw->base + reg);
}
EXPORT_SYMBOL(mtk_switch_r32);
